import React from "react";
import { connect } from "react-redux";
import { toggleSubscription } from "../actions/actions.js";

const YourComponent = ({ isSubscribed, toggleSubscription }) => {
  return (
    <div>
      <h1>User is {isSubscribed ? "subscribed" : "not subscribed"}</h1>
      <button onClick={toggleSubscription}>
        {isSubscribed ? "Unsubscribe" : "Subscribe"}
      </button>
    </div>
  );
};

const mapStateToProps = (state) => {
  return {
    isSubscribed: state.isSubscribed,
  };
};

export default connect(mapStateToProps, { toggleSubscription })(YourComponent);
