import React from "react";
import "./index.css";
import JoinUsSection from "./joinOurProgramSection";
import CommunitySection from "./CommunitySection";
import { Provider } from "react-redux";
import { configureStore } from "@reduxjs/toolkit";
import { getDefaultMiddleware } from "@reduxjs/toolkit";
import rootReducer from "./Redux/reducers/rootReducer";
import thunk from "redux-thunk";

const store = configureStore({
  reducer: rootReducer,
  middleware: [...getDefaultMiddleware(), thunk],
  devTools: process.env.NODE_ENV !== "production",
});

function App() {
  return (
    <Provider store={store}>
      <JoinUsSection />
      <CommunitySection />
    </Provider>
  );
}

export default App;
