import { combineReducers } from "redux";
import usersReducer from "../usersReducer";
import subscriptionReducer from "../subscribtionReducer";
import visibilityReducer from "../visibilityReducer";

const rootReducer = combineReducers({
  users: usersReducer,
  subscription: subscriptionReducer,
  visibility: visibilityReducer,
});

export default rootReducer;
