import { TOGGLE_SECTION_VISIBILITY } from "../actions/actions.js";

const initialState = true;

const visibilityReducer = (state = initialState, action) => {
  switch (action.type) {
    case "TOGGLE_SECTION_VISIBILITY":
      return {
        ...state,
        isVisible: !state.isVisible,
      };
    default:
      return state;
  }
};

export default visibilityReducer;
