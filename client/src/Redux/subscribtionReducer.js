import { TOGGLE_SUBSCRIPTION } from "../actions/actions";
const initialState = {
  isSubscribed: false,
};

const subscriptionReducer = (state = initialState, action) => {
  return {
    ...state,
    isSubscribed: !state.isSubscribed,
  };
};

export default subscriptionReducer;
