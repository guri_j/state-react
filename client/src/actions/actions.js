export const TOGGLE_SUBSCRIPTION = "TOGGLE_SUBSCRIPTION";
export const TOGGLE_SECTION_VISIBILITY = "TOGGLE_SECTION_VISIBILITY";
export const toggleSubscription = () => {
  return {
    type: TOGGLE_SUBSCRIPTION,
  };
};

export const toggleSectionVisibility = () => {
  return {
    type: TOGGLE_SECTION_VISIBILITY,
  };
};
